import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys
import getopt
import operator
from scipy import ndimage
import time


blockSize = 2
window_size = 5
k = 0.04
thresh = 255


# nacitanie obrazku a jeho vyhodnotenie
# pri chybe vrati None
# pri spravnom nacitani nam vrati obrazok
def readImage(filename):
    img = cv2.imread(filename, 0)
    if img is None:
        print('Invalid image:' + filename)
        return None
    else:
        print('Image successfully read...')
        return img

# hladanie hran pricom vstupom je obrazok a paramatre Harris corner detektoru
def findCorners(img, window_size, k, thresh):
    # hladanie derivacii X a Y
    dx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)
    dy = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)
    Ixx = dx**2
    Ixy = dy*dx
    Iyy = dy**2
    # vyska a sirka obrazku
    height = img.shape[0]
    width = img.shape[1]

    cornerList = []
    newImg = img.copy()
    color_img = cv2.cvtColor(newImg, cv2.COLOR_GRAY2RGB)
    offset = window_size/2

    hoff = height-offset
    hoff = int(hoff)
    woff = width-offset
    woff = int(woff)
    offset = int(offset)
    
    # loop na hladanie corners
    for y in range(offset, hoff):
        for x in range(offset, woff):
            #Calculate sum of squares
            windowIxx = Ixx[y-offset:y+offset+1, x-offset:x+offset+1]
            windowIxy = Ixy[y-offset:y+offset+1, x-offset:x+offset+1]
            windowIyy = Iyy[y-offset:y+offset+1, x-offset:x+offset+1]
            Sxx = windowIxx.sum()
            Sxy = windowIxy.sum()
            Syy = windowIyy.sum()

            # Najdenie determinantu na hladanie rohu
            det = (Sxx * Syy) - (Sxy**2)
            trace = Sxx + Syy
            r = det - k*(trace**2)

            # if r > thresh, tak pridaj hranu do listu
            # na konci return obrazku s hranami a list hran
            if r > thresh:
                cornerList.append([x, y, r])
                color_img.itemset((y, x, 0), 0)
                color_img.itemset((y, x, 1), 0)
                color_img.itemset((y, x, 2), 255)
    return color_img, cornerList


def main():
    img_name = sys.argv[1]
    img = readImage(img_name)
    if img is not None:
        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        if len(img.shape) == 4:
            img = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
        finalImg, cornerList = findCorners(img, int(window_size), float(k), int(thresh))
        if finalImg is not None:
            cv2.imwrite("finalimage.png", finalImg)


if __name__ == "__main__":
    # start timer
    t0 = time.time()
    for i in range (0,1000):
        main()
        print("Finish main " + str(i))
    # stop timer
    t1 = time.time()
    print(t1-t0)
