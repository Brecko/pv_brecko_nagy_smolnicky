import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys
import time

blockSize = 2
kSize = 5
k = 0.04

# start timer
t0 = time.time()

for i in range (0,1000):
    # nacitanie obrazku z argumentu a prevedenie do sedej
    img = cv2.imread(sys.argv[1])
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    operatedImage = np.float32(gray)
      
    # detekovanie hran pomocou cornerHarris
    dest = cv2.cornerHarris(operatedImage, blockSize, kSize, k)
      
    dest = cv2.dilate(dest, None)
      
    # Reverting back to the original image,
    # with optimal threshold value
    img[dest > 0.01 * dest.max()]=[0, 0, 255]
      
    # zobrezenie output obrazku
    #cv2.imshow('Image with Borders', img)
    if img is not None:
        cv2.imwrite("functionImage.png", img)
      
    # De-allocate any associated memory usage
    if cv2.waitKey(0) & 0xff == 27:
        cv2.destroyAllWindows()
        
    print("Finish main " + str(i))

# stop timer
t1 = time.time()
print(t1-t0)

