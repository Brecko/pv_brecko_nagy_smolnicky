Harris Cornerova detekcia hrán a jeho implementácia
Algoritmus detekcie hrán Harris, známy aj ako Harris & Stephens dektor je jedným z najjednoduchších dostupných detektorov hrán pri rastrovej grafike. Cieľom je nájsť miesta na obrázku, kde sa nachádzajú hrany, ktorých zlom pokračuje v dvoch alebo viacerých smeroch. Základnou myšlienkou algoritmu je nájsť rozdiel v intenzite posunu (u, v) vo všetkých smeroch, ktorý je vyjadrený ako:

 ![image](https://miro.medium.com/max/806/0*6i-w-2n7p5Gxs6cv.jpg)
 
Funkcia window je buď obdĺžniková alebo Gaussová, ktorá dáva váhy pixelom pod nimi.
Túto funkciu E (u, v) musíme maximalizovať na detekciu hrany. To znamená, že musíme maximalizovať druhý termín. Ak použijeme Taylorovu expanziu na vyššie uvedenú rovnicu a použijeme niektoré matematické kroky, získame konečnú rovnicu ako:

![image1](https://miro.medium.com/max/672/0*FIdvkHWmkOZCpVGX.jpg)

Kde,
 
![image2](https://miro.medium.com/max/478/0*piogPZ-2vaYuDQiG.jpg)
 
Ix a Iy sú derivátmi obrázkov v smere x a y. (Dá sa ľahko zistiť pomocou cv2.Sobel ()).
A teraz hlavná časť. Po predošlých úlohách sme vytvorili skóre, v podstate rovnicu, ktorá určí, či okno môže obsahovať roh alebo nie.
 
Kde,

![image3](https://miro.medium.com/max/466/0*TcmYywgDfz3lkN8v.jpg)
 

Hodnoty týchto vlastných hodnôt teda rozhodujú, či sa jedná o roh, hranu alebo rovnú plochu. Názornú ukážku môžeme vidieť na nasledujúcom obrázku:

![image4](https://www.muthu.co/wp-content/uploads/2018/09/Snip20180930_32.png)
 
Výsledkom detekcie Harris Corner je teda obrázok s týmito hodnotami. 
OpenCV má na tento účel funkciu cv2.cornerHarris (). Jeho argumenty sú:

img - Vstupný obrázok by mal byť v odtieňoch sivej a float32.
blockSize - Je to veľkosť protiľahlej strany uvažovaná pre detekciu rohu
ksize - Parameter clony použitého derivátu Sobel.
k - Parameter bez Harrisovho detektora v rovnici.
Proces algoritmu Harris Corner detekcie
1.    Prevedenie farebného obrázka do odtiene šedej
2.    Výpočet priestorového derivátu
3.    Nastavenie tenzora štruktúry
4.    Výpočet Harrisovej reakcie
5.    Nájdite hrán a rohov

